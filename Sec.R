library(XBRL)

inst <- "http://edgar.sec.gov/Archives/edgar/data/21344/00000213
4414000008/ko-20131231.xml"
options(stringsAsFactors = FALSE)
xbrl.vars <-
        xbrlDoAll(inst, cache.dir = "XBRLcache", prefix.out = NULL)

xbrl.vars <-
        xbrlDoAll(inst, cache.dir = "XBRLcache", prefix.out = "out", verbose =
                          TRUE)

str(xbrl.vars, max.level = 1)

library(dplyr)

head(xbrl.vars$context)